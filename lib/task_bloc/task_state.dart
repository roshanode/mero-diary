part of 'task_bloc.dart';

abstract class TaskState extends Equatable {}

class TaskInitialState extends TaskState {
  @override
  List<Object> get props => [];
}

class TaskLoadingState extends TaskState {
  @override
  List<Object> get props => [];
}

class TaskAddedState extends TaskState {
  final String message;
  TaskAddedState({
    @required this.message,
  });
  @override
  List<Object> get props => [message];
}

class TaskUpdatedState extends TaskState {
  final bool updated;
  TaskUpdatedState({
    @required this.updated,
  });
  @override
  List<Object> get props => [updated];
}

class TaskDeletedState extends TaskState {
  final bool deleted;
  TaskDeletedState({
    @required this.deleted,
  });
  @override
  List<Object> get props => [deleted];
}

class TaskLoadedState extends TaskState {
  final List<TaskModel> taskModel;
  TaskLoadedState({
    @required this.taskModel,
  });

  @override
  List<Object> get props => [taskModel];
}

class TaskErrorState extends TaskState {
  final String errorMessage;
  TaskErrorState({
    @required this.errorMessage,
  });
  @override
  List<Object> get props => [errorMessage];
}
